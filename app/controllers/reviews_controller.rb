class ReviewsController < ApplicationController
    before_action :find_libro
	before_action :find_review, only: [:edit, :update, :destroy]
	before_action :authenticate_user!, only: [:new, :edit]

	def new
		@review = Review.new
	end

	def create
		@review = Review.new(review_params)
		@review.libro_id = @libro.id
		@review.user_id = current_user.id

		if @review.save
			redirect_to libro_path(@libro)
		else
			render 'new'
		end
	end

	def edit
	end

	def update
		if @review.update(review_params)
			redirect_to libro_path(@libro)
		else
			render 'edit'
		end
	end

	def destroy
		@review.destroy
		redirect_to libro_path(@libro)
	end

	private

		def review_params
			params.require(:review).permit(:clasificacion, :comentario)
		end

		def find_libro
			@libro = Libro.find(params[:libro_id])
		end

		def find_review
			@review = Review.find(params[:id])
		end

end
