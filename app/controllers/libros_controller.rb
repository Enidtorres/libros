class LibrosController < ApplicationController
    before_action :find_libro, only: [:show, :edit, :update, :destroy]
    before_action :authenticate_user!, only: [:new, :edit]
    
    def index
        if params[:category].blank?
            @libros = Libro.all.order("created_at DESC")
        else
           @category_id = Category.find_by(name: params[:category]).id
            @libros = Libro.where(:category_id => @category_id).order("created_at DESC")    
        end
    end
    
    def show
        if @libro.reviews.blank?
			@average_review = 0
		else
			@average_review = @libro.reviews.average(:clasificacion).round(2)
		end  
    end
    
    def new
        @libro = current_user.libros.build
        @categories = Category.all.map{ |c| [c.name, c.id] }
    end
    
    def create 
        @libro = current_user.libros.build(libro_params)
        @libro.category_id = params[:category_id]
        
        if @libro.save
            redirect_to root_path
        else
            render 'new'
        end
    end
    
    def edit
        @categories = Category.all.map{ |c| [c.name, c.id] }
    end
    
    def update
        @libro.category_id = params[:category_id]
        if @libro.update(libro_params)
            redirect_to libro_path(@libro)
        else
            render 'edit'
        end 
    end
    
    def destroy
        @libro.destroy
        redirect_to root_path
    end
    
    private 
    
        def libro_params
            params.require(:libro).permit(:titulo, :descripcion, :autor, :category_id, :libro_img)
        end
    
        def find_libro
            @libro = Libro.find(params[:id]) 
        end
end
