class AddAttachmentLibroImgToLibros < ActiveRecord::Migration
  def self.up
    change_table :libros do |t|
      t.attachment :libro_img
    end
  end

  def self.down
    remove_attachment :libros, :libro_img
  end
end
