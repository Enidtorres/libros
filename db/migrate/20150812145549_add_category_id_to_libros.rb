class AddCategoryIdToLibros < ActiveRecord::Migration
  def change
    add_column :libros, :category_id, :integer
  end
end
