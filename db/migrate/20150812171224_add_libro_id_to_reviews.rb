class AddLibroIdToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :libro_id, :integer
  end
end
